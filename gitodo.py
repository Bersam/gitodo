#!/usr/bin/python
#-*- coding:utf-8 -*-

#Python import
import os
from sys import argv
from optparse import OptionParser

#project import
from core import todo_list, scan, enable, disable, checkGitodo
from plugins.bitbucket import addBit, removeBit
from plugins.github import addGitHub, removeGitHub


def main():
    parser = OptionParser(usage='%prog args')
    parser.add_option('-l', '--list', action="store_true", dest='list', help='list todos')
    parser.add_option('-s', '--scan', action="store_true", dest='scan', help='scan todos')
    parser.add_option('-e', '--enable', action="store_true", dest='enable', help='enable gitodo')
    parser.add_option('-d', '--disable', action="store_true", dest='disable', help='disable gitodo')

    opts, args = parser.parse_args()
    if opts.list:
            todo_list()

    elif opts.scan:
            if(checkGitodo()):
                scan()
            else:
                enable()

    elif opts.enable:
            if(not checkGitodo()):
                enable()
            else:
                print "gitodo already enabled for yor repository"

    elif opts.disable:
            if(checkGitodo()):
                disable()
    else:
        parser.print_help()
    # elif(arg == 'add' and argv[2] == 'bitbucket'):
    #         if(checkGitodo()):
    #             addBit()

    # elif(arg == 'remove' and argv[2] == 'bitbucket'):
    #         removeBit()
    # elif(arg == 'add' and argv[2] == 'github'):
    #         addGitHub()
    # elif(arg == 'remove' and argv[2] == 'github'):
    #         removeGitHub()
    # else:
    #     usage()
    # except:
    #     usage()

if __name__ == "__main__":
    main()
