#!/usr/bin/python
#-*- coding:utf-8 -*-
import subprocess
import os
import re
import hashlib
from prettytable import PrettyTable
from sys import argv
import cPickle as pickle

dbname = os.getcwd() + "/.git/db.p"
try:
    alltodo = pickle.load( open(dbname, "r") )
except:
    alltodo = []

def todo_list():
    pt = PrettyTable(["#","location","reference id","message"])
    pt.align["location"] = "l"
    for i,todo in enumerate(alltodo):
        fileName = todo["fileName"].replace(os.getcwd()+"/","")+":"+todo['lineNumber'];
        pt.add_row([i+1,fileName,todo["refid"],todo["message"]])
    print pt

def scan(dir=os.getcwd()):
    if os.path.exists(dbname):
        print "Scanning last commit diff"
        cmd = "git show --pretty=\"format:\"  --name-only"
        results = subprocess.check_output(cmd,shell=True)
        for line in results.splitlines():
            if line:                
                filelocation = dir+"/"+line.replace(" ", "\ ")
                if not os.path.exists(filelocation):
                    break
                cmd = "find %s -type f -exec grep -EHin \"^\s*(\#|//|--)(\s+)?@?todo(\s|:)\" {} \;"% filelocation
                res = subprocess.check_output(cmd, shell=True)
                for line in res.splitlines():
                    processLine(line)
    else:
        print "Scanning " + dir
        cmd = "find " + dir +" -type f -exec grep -EHin \"^\s*(\#|//|--)(\s+)?@?todo(\s|:)\" {} \;"
        results = subprocess.check_output(cmd, shell=True)
        print str(results.count("\n")) + " Item(s) found"
        for line in results.splitlines():
            processLine(line)
    pickle.dump( alltodo, open( dbname, "w" ) )
    print "Scan done."

def processLine(line):
    print "Proccessing Line : "+ line
    results = re.findall("(.+):(.+):.+@?todo[\s|:]\s*(.+)",line,re.IGNORECASE)
    for todo in results:
        exist = proccessTodo(todo)
        if not exist:
            alltodo.append({
                "fileName"     : todo[0],
                "lineNumber"   : todo[1],
                "message"      : todo[2],
                "hash"         : hashlib.md5(todo[0]+todo[2]).hexdigest(),
                "refid"        : ""
            })
            return True

def proccessTodo(todo):
    ddd = []
    founded = False
    for i in range(len(alltodo)):
        if hashlib.md5(todo[0]+todo[2]).hexdigest() == alltodo[i]["hash"]:
            ddd.append(i)
            founded = True
    if founded :
        for i in ddd[::-1]:
            del alltodo[i]
        return True
    return False

def enable(dir=os.getcwd()):
    if(os.path.isdir(dir+"/.git")):
        print "trying to enable gitodo for your repository ... "
        #add hook to the repository
        if(not os.path.exists(dir+"/.git/hooks/post-commit")):
            #create post-commit hook
            with open(dir+"/.git/hooks/post-commit", "w") as f:
                f.write("#!/bin/sh"+"\n")
                f.write("gitodo --scan")
            print "Done"
            os.chmod(dir+"/.git/hooks/post-commit", 0777)
        else:
            #append to post-commit hook
            with open(dir+"/.git/hooks/post-commit", "a") as f:
                f.write("gitodo scan")
        scan()

def disable(dir=os.getcwd()):
    print "disable gtodo for this repo"
    hookIn= open(dir+"/.git/hooks/post-commit")
    hookOut= open(dir+"/.git/hooks/post-commit",'w')
    line=""
    for line in hookIn:
       line = line.replace("gitodo scan", "")
    hookOut.write(line)
    hookIn.close()
    hookOut.close()
    #remove db.p
    os.remove(dir+"/.git/db.p")


def checkGitodo(dir=os.getcwd()):
    if(not os.path.isdir(dir+"/.git")):
        print "this isn't a git repository"
        return False
    if(not os.path.exists(dir+"/.git/hooks/post-commit") or not('gitodo' in open(dir+"/.git/hooks/post-commit").read())):
        print "gitodo is not enabled in this repository"
        return False
    return True
